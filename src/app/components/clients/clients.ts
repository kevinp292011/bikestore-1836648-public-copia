export interface IClient {
    id?: number,
    clientName?: string,
    documentNumber?: string,
    clientEmail?: string,
    phoneNumber?: string,   
}
