import { Component, OnInit } from '@angular/core';
import { BikesService } from '../bikes.service';
import { IBike } from '../bike.interface';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bikes-list',
  templateUrl: './bikes-list.component.html',
  styleUrls: ['./bikes-list.component.sass']
})

export class BikesListComponent implements OnInit {
  bikeList: IBike[];
  bike: IBike;
  existData = false; 
  searchForm = this.formBuilder.group({
      serial: '',
      model : ''
  });
  pageSize = 10;
  pageNumber = 0;
  countPages = 0;
  listPages = [];

  toggleButtonShowForm = false;

  filters = {
    Size: 10,
    Number: 0,
    serial: '',
    model: ''
  };

  constructor(private bikesService: BikesService,
              private formBuilder: FormBuilder,
              private router: Router
              ) { }

  ngOnInit() {
    this.initQueryPages(0);
  }

  initQueryPages(page: number): void {
    this.pageNumber = page;
    this.createFilterParams();
    this.bikesService.query(this.filters)
    .subscribe((res: any) => {
      this.bikeList = res;
      console.log('List Data', this.bikeList);
      this.formatPagination(res.totalPages)
    }, err => console.error(err));
  }

  searchBykeBySerial(): void {
    console.warn('data input',this.searchForm.value);
    this.bikesService.getBikeBySerial(this.searchForm.value.serial)
    .subscribe(res => {
      console.warn('GET DATA ',res);
      
      this.bike = res;
      if(!res) {
        this.existData = true;
      } else {
        this.existData = false;
      }

    }, error => {
      console.warn('Error ',error);
      this.existData = true;
    });

  }

  deleteItem(id: string): void {
    console.warn('Event Click ID: ',id);
    this.bikesService.deleteItem(id)
    .subscribe(res => {
      this.ngOnInit();
    },err => {
      console.log('Error al eliminar');
      
    });  
  }


  filterData(): void {
    this.createFilterParams();
    console.warn('Filers ', this.filters);
    this.bikesService.query(this.filters)
    .subscribe((res: any) => {
      // llegan los datos filtrados
    });
   // this.initQueryPages(0);
  }

 private createFilterParams(): void {
    if (this.searchForm.value.model) {
      this.filters.model = this.searchForm.value.model;
    } else {
      delete this.filters.model;
    }
    if (this.searchForm.value.serial) {
      this.filters.serial = this.searchForm.value.serial;
    } else {
      delete this.filters.serial;
    }
  }


  changeStatusBike(item: IBike): void {
    console.warn('SELECTED BIKE ', item);
    this.bikesService.updateBike(item)
    .subscribe(res => {
      console.warn('Estado de la bicicleta actualizado...');
    });
  }

  loadComponentUpdate(id: number): void {
    this.router.navigate(['/bikes/bikes-update', id]); // Redireccionar
  }

  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for (let i = 0; i < totalPages; i++){
      this.listPages.push(i);
    }
  }
}