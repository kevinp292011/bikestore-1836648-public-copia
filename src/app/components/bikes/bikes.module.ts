import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BikesListComponent } from './bikes-list/bikes-list.component';
import { BikesRoutingModule } from './bikes-routing.module';
import { BikesCreateComponent } from './bikes-create/bikes-create.component';
import { ReactiveFormsModule} from '@angular/forms';
import { BikesUpdateComponent } from './bikes-update/bikes-update.component';
import {PaginatorModule} from 'primeng/paginator';
import { InputSwitchModule } from 'primeng/inputswitch';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';


@NgModule({
  declarations: [
    BikesListComponent,
    BikesCreateComponent,
    BikesUpdateComponent
  ],
  imports: [
    CommonModule,
    BikesRoutingModule,
    ReactiveFormsModule,
    PaginatorModule,
    InputSwitchModule,
    MessageModule,
    MessagesModule
  ]
})
export class BikesModule { }
