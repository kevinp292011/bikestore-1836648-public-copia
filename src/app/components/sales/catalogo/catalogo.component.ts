import { Component, OnInit } from '@angular/core';
import { SalesService } from '../sales.service';
import { BikesService } from '../../bikes/bikes.service';
import { ISale } from '../sales';
import { ClientsService } from '../../clients/clients.service';
import { IClient } from '../../clients/clients';
import { IBike } from '../../bikes/bike.interface';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.styl']
})
export class CatalogoComponent implements OnInit {
  bikeList: IBike[];
  salesListCar: ISale[] = [];
  clientsList: IClient[] = [];
  
  selectedClientTemp: IClient;

  total: number = 0;

  searchForm = this.fb.group({
    document: '',
    lastName: ''
  });
  pageSize = 10;
  pageNumber = 0;
  countPages = 0;
  listPages = [];


  constructor(
    private salesService: SalesService,
    private bikesService: BikesService,
    private clientService: ClientsService,
    private fb: FormBuilder
    ) {

    }

  ngOnInit(): void {
    this.initQueryPages(0);

  }

  initQueryPages(page: number): void {
    this.pageNumber = page;
    this.bikesService.query({
      Size: this.pageSize,
      page: this.pageNumber
    })
    .subscribe((res: any) => {
      this.bikeList = res;
      console.log('List Data', this.bikeList);
      this.formatPagination(res.totalPages)
    }, err => console.error(err));
  }
  addClientToSale(client: IClient): void {
    console.warn('EVENT SELECTED ', client);
    this.selectedClientTemp = client;
    console.warn('Selecte Client Temp ',this.selectedClientTemp);
  }

  searchClient(): void{
    console.warn('Load data');
    this.clientService.query({
     'document.contains': this.searchForm.value.document,
     'lastName.conatins': this.searchForm.value.lastName
    })
    .subscribe(res => {
      this.clientsList = res;
    });
  }

  addToCar(bike: IBike): void {
    console.warn('Selected ',bike);
    this.total += bike.price;
    this.salesListCar.push({
        client: this.selectedClientTemp,
        bike: bike
      });
      console.warn('LISTO PARA ENVIAR AL BACKEND ',this.salesListCar);
  }


  searchClientAutocomplete(event: any): void {
      console.warn('data',event);
      this.clientService.findClientByDocument({
        'document.contains': event.query
      })
      .subscribe(res => {
        this.clientsList = res;
        console.warn('DATA GET AUTO ',this.clientsList);
      })
  }
  
  private formatPagination(totalPages: number): void {
    this.listPages = [];
    for(let i = 0; i < totalPages; i++){
      this.listPages.push(i);
    }
  }
  
  confirmSale():void {
    this.salesService.createMultiple(this.salesListCar)
    .subscribe((res: any) => {
      console.warn('Save ok ',res);
    });

  }
}
