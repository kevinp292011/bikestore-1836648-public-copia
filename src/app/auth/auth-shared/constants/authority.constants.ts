export const enum Authority{
    USER = 'ROLE_USER',
    ADMIN = 'ROLE_ADMIN',
    CLIENT = 'ROLE_CLIENT'
}