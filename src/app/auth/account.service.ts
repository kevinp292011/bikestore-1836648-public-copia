import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { Account } from './auth-shared/models/account.model';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { catchError, shareReplay, tap } from 'rxjs/operators';
import { Authority } from './auth-shared/constants/authority.constants';
import { ICredentials } from './auth-shared/models/credentials';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private authenticationState = new ReplaySubject<Account | null>(1);
  private userIdentity: Account | null = null;
  private accountCache?: Observable<Account | null>;

  constructor(
    private http: HttpClient, private router: Router
  ) { }


  /**
   * Create User
   * @param account  
   */

  create(credentials: ICredentials): Observable<ICredentials> {
    return this.http.post<ICredentials>(`${environment.END_POINT}/api/account`, credentials);
  }
  /* Create User*/

  identity(force?: boolean): Observable<Account | null> {
    if (!this.accountCache || force || !this.isAuthenticated()) {
      this.accountCache = this.fetch().pipe(catchError(() => {
        return of(null);
      }), tap((account: Account | null) => {
        this.authenticate(account);
        /*if (account) {
          this.router.navigate(['/dashboard/bikes']);
        }*/
      }), shareReplay());
    }

    return this.accountCache;
  }

  authenticate(identity: Account | null): void {
    this.userIdentity = identity;
  }

  isAuthenticated(): boolean {
    return this.userIdentity !== null;
  }

  getUserName(): string {
    return this.userIdentity.lastName;
  }

  getAuthenticationState(): Observable<Account | null> {
    return this.authenticationState.asObservable();
  }

  /*
   * @param authorities verify rols to users
   */
  hasAnyAuthority(authorities: string[] | string): boolean {
    if (!this.userIdentity || !this.userIdentity.authorities) {
      return false;
    }
    if (!Array.isArray(authorities)) {
      authorities = [authorities];
    }
    return this.userIdentity.authorities.some((authority: string) => authorities.includes(authority));
  }

  private fetch(): Observable<Account> {
    return this.http.get<Account>(`${environment.END_POINT}/api/account`);
  }
}