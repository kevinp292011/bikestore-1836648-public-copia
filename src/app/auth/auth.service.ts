import { Injectable } from '@angular/core';
import { ICredentials } from '../auth/auth-shared/models/credentials';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }
  public login(credentials: ICredentials): Observable<any>{
    const data: ICredentials = {
      username: credentials.username,
      password: credentials.password,
      rememberMe: credentials.rememberMe
    }
    return this.http.post<any>(`${environment.END_POINT}/api/authenticate`, data, {observe: 'response'})
    .pipe(map(res =>{
      const bearerToken = res.headers.get('Authorization');
      console.log('VER TOKEN ', bearerToken);
      if (bearerToken && bearerToken.slice(0, 7) === 'Bearer '){
        const jwt = bearerToken.slice(7, bearerToken.length);
        this.storeAuthenticationToken(jwt, credentials.rememberMe)
        
        return jwt;
      }
    }));
  }

  logout(): Observable<any>{
    return new Observable(observe => {
      localStorage.removeItem('token')
      sessionStorage.removeItem('token');
      observe.complete();
    })
  }

  private storeAuthenticationToken(jwt: string, rememberMe: boolean): void{
    if(rememberMe){
      localStorage.setItem('token', jwt);
    }else{
      sessionStorage.setItem('token',  jwt);
    }
  }
}