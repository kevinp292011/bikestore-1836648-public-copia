import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ICredentials, Credentials} from '../auth-shared/models/credentials';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {


  
loginForm: FormGroup;


  constructor( private fb: FormBuilder, private loginService: LoginService, private router: Router) {
    this.loginForm = this.fb.group({
      username:['', [Validators.required]],
      password:['', [Validators.required]],
      rememberMe:[''],
    });
  

   }

  ngOnInit(): void {
  }


 


  login(): void{
    console.warn('DATOS ', this.loginForm.value);
    const credentials: ICredentials = new Credentials();

    credentials.username = this.loginForm.value.username;
    credentials.password = this.loginForm.value.password;
    credentials.rememberMe = this.loginForm.value.rememberMe;

    this.loginService.login(credentials)
    .subscribe((res: any) => {
      console.warn('DATA LOGIN CONTROLLER - OK Login ', res);
      this.router.navigate(['/dashboard']);
    },
    (error: any) => {
      if(error.status === 400){
        console.warn('Usuario o constraseña incorrectos');
      }
    });
  }


}
