import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { Authority } from '../auth/auth-shared/constants/authority.constants';
import { BikesListComponent } from '../components/bikes/bikes-list/bikes-list.component';
import { UserRouteAccessService } from '../auth/guards/user-route-access.service';



const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'bikes',
        data: {
          authorities: [Authority.ADMIN, Authority.USER]
        },
        canActivate: [UserRouteAccessService],
        loadChildren: () => import('../components/bikes/bikes.module')
        .then(m => m.BikesModule)
      },
      {
        path: 'sales',
        data: {
          authorities: [Authority.ADMIN]
        },
        canActivate: [UserRouteAccessService],
        loadChildren: () => import('../components/sales/sales.module')
        .then(m => m.SalesModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
