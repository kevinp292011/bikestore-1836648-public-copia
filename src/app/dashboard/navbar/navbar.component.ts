import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { AccountService } from 'src/app/auth/account.service';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass'],
  providers: [ConfirmationService]
})
export class NavbarComponent implements OnInit {
  fullName: string;
  constructor(
    private router:Router,
    private authService: AuthService,
    private accountService: AccountService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit(): void {
   this.fullName = this.accountService.getUserName();
  }
  public cerrarSesion(): void {
    this.router.navigate(['login'])
    this.authService.logout().subscribe(null, null, () => this.accountService.authenticate(null));
    console.log('Se eliminó el token')
  }

  confirm() {
    this.confirmationService.confirm({
      message: 'Seguro que deseas cerrar sesion',
      accept: () => {
        this.cerrarSesion();
      },
      reject: () => {
        console.log('cancelado');
      } 
    });
  }

}
