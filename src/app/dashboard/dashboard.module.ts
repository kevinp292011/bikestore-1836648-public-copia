import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { MainComponent } from './main/main.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { AuthSharedModule } from '../auth/auth-shared/auth-shared.module';


@NgModule({
  declarations: [MainComponent, SidebarComponent, FooterComponent, NavbarComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ConfirmDialogModule,
    AuthSharedModule
  ]
})
export class DashboardModule { }
